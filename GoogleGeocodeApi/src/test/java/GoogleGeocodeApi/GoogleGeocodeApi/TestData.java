package GoogleGeocodeApi.GoogleGeocodeApi;





import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Scanner;

import com.google.common.base.Splitter;

import com.opencsv.exceptions.CsvValidationException;





import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class TestData {
	
	
		public Map<String, String> getTestData(String fileName, String testCaseId) throws URISyntaxException
	    {
	          String filePath = null;
	          Map<String, String> map = null;

	          try {
//	                filePath = Paths.get(getClass().getClassLoader()
//	                            .getResource("dataproviders\\CohuUI\\" + fileName).toURI());
	        	  
	        	  filePath = System.getProperty("user.dir")+"\\"+fileName;
	                
	                Scanner scanner = new Scanner(new File(filePath));
	            
	                while (scanner.hasNext()) {
	                      map = Splitter.on(",").trimResults().withKeyValueSeparator("=").split(scanner.nextLine());
//	                	map = scanner.nextLine();
//	                      System.out.println(map.get("testCaseId"));
	                      if (map.get("testCaseId").trim().equals(testCaseId)) {
	                    	
	                            break;
	                      }
	                }

	            scanner.close();
	                
	          } catch (Exception e) {
	                System.out.println("Exception while reading the file " + e.getMessage());
	                e.printStackTrace();
	                
	          }
	          
	          return map;
	    }
		

		public void readCSV(String fileName) throws CsvValidationException, IOException {
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\"+fileName);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			
			
			
		}

}

