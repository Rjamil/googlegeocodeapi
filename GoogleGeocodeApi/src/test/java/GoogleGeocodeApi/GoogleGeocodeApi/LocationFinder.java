package GoogleGeocodeApi.GoogleGeocodeApi;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import org.testng.annotations.Test;

import com.opencsv.exceptions.CsvValidationException;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class LocationFinder {
	
	
//	private String readJSON(String fileName) {
//
//		String jsonRequest = "";
//		Path filePath = null;
//
//		try {
//			filePath = Paths.get(getClass().getClassLoader()
//					.getResource("dataproviders//" + "qa.auto.preissue.AppsTx121API//" + fileName).toURI());
//			jsonRequest = new String(Files.readAllBytes(filePath));
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			System.out.println("IOException while reading the file "  + e1.getMessage());
//			e1.printStackTrace();
//			
//			
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			System.out.println("URISyntaxException while reading the file "  + e.getMessage());
//			e.printStackTrace();
//		}
//		return jsonRequest;
//
//	}  
	

	
	@Test
	public void findLocationWithGeoCode() throws URISyntaxException {
		
//		 String json = readJSON("invalidDOBFormat.json");
		TestData testdata = new TestData();
		
		for(int i=0; i<=462; i++) {
			String iValue = String.valueOf(i) ;
		String latitude = testdata.getTestData("test.csv", iValue).get("Lat");
		String longitude = testdata.getTestData("test.csv", iValue).get("Long");
		
		System.out.println("Latitude = "+latitude + "   ||   " +"longitude = "+ longitude);
		
		
		
		
		
		
//		String latitude = "26";
//		String longitude = "30";
		 
//	     RestAssured.baseURI = "";
		 Response res =  given().
						        
						 when().
								get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&key=AIzaSyAZwhzbFnZvG3xGRF3GBLJmg_BFgQqj51o").
						 then().
								assertThat().statusCode(200).and().
								extract().response();

//		given().when().get("http://www.google.com").then().statusCode(200);
		 		
		 String address = null;
			    
			    if(res.jsonPath().getString("results.formatted_address[1]")==null) {
			    	
			    	address = res.jsonPath().getString("results.formatted_address[0]");
			    	System.out.println("formatted_address[1] was null,so address is formatted_address[0]!");
			    }
			    else if(res.jsonPath().getString("results.formatted_address[1]").contains(",")) {
			    	
			    	address = res.jsonPath().getString("results.formatted_address[1]").replace(","," ");
			    	System.out.println("formatted_address[1] printed");
			    }else {
			    	
			    	address = "Look up Address manually";
			    	System.out.println("Look up address manually");
			    }
			    
			   
//			    address = res.jsonPath().getString("plus_code.compound_code");
			   
			    
			    System.out.println(address);
			    
			    saveRecord(latitude, longitude, address, filepath);   
//		Assert.assertEquals(res.jsonPath().getString("error"), "Internal Server Error");
		
		}
		
	}
	
	
	@Test
	public void readExcel() throws IOException, URISyntaxException, CsvValidationException {
		
		TestData testdata = new TestData();
		
		for(int i=0; i<=462; i++) {
			String iValue = String.valueOf(i) ;
		String latitude = testdata.getTestData("test.csv", iValue).get("Lat");
		String longitude = testdata.getTestData("test.csv", iValue).get("Long");
		
		System.out.println("Latitude = "+latitude + "   ||   " +"longitude = "+ longitude);
		
//		saveRecord(latitude, longitude, filepath);
		
		}
		
//		testdata.readCSV();
		
	}
	
	String ID = "1234";
	String name = "Bob";
	String age = "22";
	String filepath = "address.csv";
	
	public void saveRecord (String latitude, String longitude,String address, String filepath) {
		
		try {
			
		FileWriter fw  =  new FileWriter(filepath,true);
		BufferedWriter bw =  new BufferedWriter(fw);
		PrintWriter pw = new PrintWriter(bw);
		pw.println(latitude+","+longitude+","+address);
		pw.flush();
		pw.close();
		
//		JOptionPane.showMessageDialog(null,"Record saved");
			
		} 
		catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null,"Record not saved");
		}
	}
	
	@Test
	public void run() {
		
//		saveRecord(ID, name, age, filepath);
	}

}
